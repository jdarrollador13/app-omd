<?php
require_once '../models/request.php';
require_once '../models/consults.php';
$access = $_REQUEST['accion'];
$resJson;
$methods = $_SERVER['REQUEST_METHOD'];
try {
	if($methods == 'GET') {

		switch ($access) {
			case 'getUsers':
				$getConsult = new Consults();
				$resJson = $getConsult->getUsers();
			break;
			case 'getAllData':
				$getConsult = new Consults();
				$resJson = $getConsult->getAllData();
			break;
		}

	}else if($methods == 'POST') {

		$addRequest= new Request(json_decode(file_get_contents('php://input'), true));
		$addRequest->getREquest();
		switch ($access) {
			case 'validate':
				$getConsult = new Consults();
				$resJson = $getConsult->authUsers($addRequest);
			break;
			case 'editUser':
				$getConsult = new Consults();
				$resJson = $getConsult->updateUsers($addRequest);
			break;
			case 'addUser':
				$getConsult = new Consults();
				$resJson = $getConsult->createUser($addRequest);
			break;
		}

	}
	
} catch (Exception $e) {
	//echo json_encode($e);
} finally {
	//var_dump($resJson);
	echo json_encode($resJson);
}