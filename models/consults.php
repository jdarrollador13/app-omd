<?php 
require_once 'connexion.php';

/**
 * 
 */
class Consults
{
	private $sql;
	private $query;
	private $sql2;
	private $query2;
	private $sqlValidate;
	private $queryValidate;
	private $res;
	
	function __construct()
	{
		$this->connect = new connexion();
		$this->connect->getConnection();
		# code...
	}

	public function getUsers() {

		try {
			$this->res= array();
			 $this->sql="SELECT 
										u.id AS id, 
										u.nom_usuario AS nom_usuario, 
										u.ape_usuario AS ape_usuario, 
										u.usuario AS usuario, 
										CASE WHEN  activo = 1 THEN 'Activo' ELSE 'Inactivo' END AS activo,
										u.activo AS activ,
										a.nom_area AS nom_area, 
										a.id AS id_areas,
										p.nom_perfil AS nom_perfil,
										p.id AS id_perfil
									FROM usuario AS u
									INNER JOIN areas a ON u.id_area = a.id
									INNER JOIN perfil p ON u.id_perfil = p.id";
			$this->query = $this->connect->connect->query($this->sql);
			$rows = $this->query->fetchAll(PDO::FETCH_CLASS);
			if($this->query->rowCount() > 0) {	
				$this->res['data'] = $rows;
				$this->res['status'] = 200;
			} else {
				$this->res['status'] = 201;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		}finally {
			return $this->res;
		}
	}

	public function getAllData() {

		try {
			$this->res = array();
			$this->sql = "SELECT id, nom_perfil FROM perfil";
			$this->query = $this->connect->connect->query($this->sql);

			if($this->query->rowCount() > 0) {
				$rowsPerfil = $this->query->fetchAll(PDO::FETCH_CLASS);
				$this->sql2 = "SELECT id, nom_area  FROM areas";
				$this->query2 = $this->connect->connect->query($this->sql2);
			}

			if($this->query2->rowCount() > 0){
				$rowsArea = $this->query2->fetchAll(PDO::FETCH_CLASS);
				$this->res['perfil'] = $rowsPerfil;
				$this->res['areas']  = $rowsArea;
				$this->res['status'] = 200;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		} finally {
			return $this->res;
		}

	}

	public function authUsers(Request $request){

		try {
			$this->res= array();
			$user = $request->request['userName'];
			$pass = $request->request['userPass'];
			$this->sql ="SELECT * 
									 FROM usuario 
									 WHERE usuario = '$user' 
									 AND contrasena = '$pass'
									 AND activo = 1";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if($this->query->rowCount() > 0 ){
				$data = $this->query->fetchAll();
				$this->res['data'] = $data;
				$this->res['status'] = 200;
			}else{
				$this->res['status'] = 201;
			}
		} catch (PDOException  $e) {
			$this->res['status'] = 500;
			//echo $e->getMessage();
		} finally {
			return $this->res;
		}
		
	}

	public function updateUsers(Request $request){

		try {
			
			$nom_usuario = $request->request['nom_usuario'];
			$ape_usuario = $request->request['ape_usuario'];
			$usuario     = $request->request['usuario'];
			$id_perfil   = $request->request['id_perfil'];
			$id_areas    = $request->request['id_areas'];
			$activo      = (($request->request['activ'] == 0 ) ? (string)$request->request['activ'] : $request->request['activ']);
			$id          = $request->request['id'];
			$this->res   = array();
			$this->sql = "UPDATE usuario SET nom_usuario = '$nom_usuario' , ape_usuario = '$ape_usuario', usuario = '$usuario',
													 id_perfil = $id_perfil, id_area = $id_areas, activo = $activo WHERE id = $id ";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if($this->query->rowCount() > 0 ){
				$this->res['status'] = 200;
			}else{
				$this->res['status'] = 201;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		} finally {
			return $this->res;
		}
	}

	public function createUser(Request $request){

		try {
			$this->res = array();
			$resValidate =  self::validateUsers($request);
			if($resValidate == 2 ){
				$nom_usuario   = $request->request['nom_usuario'];
				$ape_usuario   = $request->request['ape_usuario'];
				$usuario       = $request->request['usuario']; 
				$password      = $request->request['password'];
				$num_documento = $request->request['num_documento'];
				$id_perfil     = $request->request['id_perfil'];
				$id_areas      = $request->request['id_area'];
				$tipo_documento = 'Cedula';
				$id_ciudad = 1;

				$this->sql = "INSERT INTO usuario (nom_usuario,ape_usuario,usuario,contrasena,num_documento,id_perfil,id_area,tipo_documento,id_ciudad)
											VALUES 
											('$nom_usuario','$ape_usuario','$usuario','$password','$num_documento',$id_perfil,$id_areas,'$tipo_documento',$id_ciudad)";
				$this->query = $this->connect->connect->prepare($this->sql);
				$this->query->execute();
				if($this->query->rowCount() > 0){
					$this->res['status'] = 200;
					$this->res['message'] = 'ok';
				}else{
					$this->res['status'] = 201;
					$this->res['message'] = 'ok';
				}
			}else{
				$this->res['status'] = 204;
				$this->res['message'] = 'existe';
			}
			
		} catch (PDOException $e) {
			$this->res['status'] = 500;
			$this->res['message'] = 'error';
		} finally{
			return $this->res;
		}
		
	}
 /** 
  * Validar Si e l usuario ya existe *
  * @param  [type] $request [description]
  * @return [type]          [description]
  */
	private function validateUsers($request){

		try {
			$resValidate;
			$usuario  = $request->request['usuario']; 
			$password = $request->request['password'];
			$this->sqlValidate = "SELECT id, usuario FROM usuario WHERE usuario = '$usuario' AND contrasena = '$password'";
			$this->queryValidate = $this->connect->connect->prepare($this->sqlValidate);
			$this->queryValidate->execute();
			if($this->queryValidate->rowCount() > 0 ){
				$resValidate = 1;
			}else{
				$resValidate = 2;
			}
		} catch (Exception $e) {
			 $resValidate = 3;
		}finally {
			return $resValidate;
		}

	}

	public function Obtenercliente(){
		$this->sql = "SELECT * from cliente";
		$queryValidate = $this->connect->connect->query($this->sql);
		$datos = $queryValidate->fetchAll();
		//print_r($datos);
		return $datos;
		die();
		
	}
}