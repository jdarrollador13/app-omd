var app2 = new Vue({
  el: '#app-loguin',
  data: {
  	message : '',
  	error : false,
    dataUser : {
    	userName : null,
    	userPass : null
    }
  },
  methods:{
  	validateUsers: function(){
  		console.log(this.dataUser.userName, 'usuario name');
  		console.log(this.dataUser.userPass, 'usuario pass');
  		const jsonUser = {
  			'userName' : this.dataUser.userName,
  			'userPass' : this.dataUser.userPass
  		};
  		fetch('../../../app_omd/controllers/router.php?accion=validate', {
			  method: 'POST', 
			  body: JSON.stringify(jsonUser), // data can be `string` or {object}!
			  headers:{
			    'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => {
				let status = response.status;
				if(status == 200){
					this.message = '';
					this.error = false;
					window.location.href = 'views/list_user.php';
				}else if(status == 201){
					this.message = 'El Usuario o Contraseña son invalidos...';
					this.error = true;
				}else if(status == 500){
					this.message = 'Intenta mas tarde el ingreso...';
					this.error = true;
				}
			});
  	}
  }
})